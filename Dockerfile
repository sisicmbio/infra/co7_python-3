FROM registry.gitlab.com/sisicmbio/infra/co7:production as production
ARG PDCI_BASE_COMMIT_SHORT_SHA
#ARG PDCI_COMMIT_MESSAGE
ARG PDCI_BASE_COMMIT_TAG
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>
LABEL Vendor=""
LABEL PDCI_BASE_COMMIT_TAG=${PDCI_BASE_COMMIT_SHORT_SHA}
LABEL PDCI_BASE_COMMIT_SHORT_SHA=${PDCI_BASE_COMMIT_TAG}

ENV PYTHONIOENCODING=utf8
ENV APP_HOME=${APP_HOME:-/code}

WORKDIR $APP_HOME

RUN cat /etc/pki/ca-trust/source/anchors/CA_ICMBIO.crt \
    /etc/pki/ca-trust/source/anchors/root_3.crt \
    /etc/pki/ca-trust/source/anchors/root_2.crt \
    /etc/pki/ca-trust/source/anchors/root_1.crt \
    /etc/pki/ca-trust/source/anchors/icpbrasilv10.crt \
    /etc/pki/ca-trust/source/anchors/serprossl.crt \
    /etc/pki/ca-trust/source/anchors/lets-wild-icmbio-chain.pem \
    > /etc/pki/ca-trust/source/anchors/bundle.ca

# Instalação Python 3
RUN yum --setopt=tsflags=nodocs install python36 -y && \
    yum --setopt=tsflags=nodocs install python36-devel -y && \
    yum --setopt=tsflags=nodocs install python36-pip -y && \
    yum clean all && \
    rm -rf /var/cache/yum

EXPOSE 8000

COPY src/ $APP_HOME

# Instalação App Django
RUN python3 -m pip install -r requirements/production.txt
ENV PYTHONPATH=/code/testproject
CMD ["gunicorn", "testproject.wsgi", "-b", "0.0.0.0:8000"]

FROM production as testing
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>

RUN python3 -m pip install -r requirements/development.txt
CMD ["python3", "testproject/manage.py", "runserver", "0.0.0.0:8000"]
