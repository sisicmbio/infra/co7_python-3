# dockerfiles-centos-python3

CentOS Dockerfile for Python 3


## Build

Copy the sources down and do the build

```
# docker build --rm -t <username>/python3 .
```

## Usage

To run:

```
# docker run -it <username>/python3 
```

## Test
